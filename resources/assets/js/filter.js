import moment from 'moment';
import Vue from 'vue';
Vue.filter('timeFormate',(arg)=>{
    // return moment(arg).startOf('hour').fromNow();
    return moment(arg).format('MMMM Do YYYY, h:mm:ss a');
});

Vue.filter('sortlength',function (text,length,suffix) {
    return text.substring(0,length)+suffix;
})