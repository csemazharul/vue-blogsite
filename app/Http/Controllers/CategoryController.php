<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return response()->json([
            'categories'=>$categories
        ],200);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'cat_name'=>'required|min:5,max:40'
        ]);
        $data=$request->all();
        Category::create($data);
        return ['message'=>'ok'];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categoryEditData=Category::findOrfail($id);
        return response()->json(['categoryEditData'=>$categoryEditData],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $categoryId=Category::findOrfail($id);
        $categoryId->update($request->all());
        return "Sucees Fully Update";

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category=Category::findOrfail($id);
        $category->delete();
        return response()->json(['message'=>'Delete Successfully'],200);
    }

    public function selectedCategory($ids)
    {
        $arrayIds=explode(',',$ids);
        foreach ($arrayIds as $id)
        {
            $categoryId=Category::findOrfail($id);
            $categoryId->delete();
        }
        return response()->json(
            ['message'=>'Success Selected Deleted'],200
        );

    }
}
