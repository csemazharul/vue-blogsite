require('./bootstrap');
window.Vue = require('vue');

// import Vue from 'vue';

import Vuex from 'vuex'

Vue.use(Vuex);

import storeData from './store/index'

const store=new Vuex.Store(
    storeData
);

import VueRouter from 'vue-router';
Vue.use(VueRouter);


import adminmain from './components/admin/AdminMaster.vue';
import homemain from './components/public/PublicMaster.vue';





import {routes} from "./routes";
import {filter} from "./filter";



//


import {
    Form,
    HasError,
    AlertError,
    AlertErrors,
    AlertSuccess
}
from 'vform'
window.Form=Form;
Vue.component(HasError.name, HasError);
Vue.component(AlertError.name, AlertError);
Vue.component(AlertErrors.name, AlertErrors);
Vue.component(AlertSuccess.name, AlertSuccess);
// ES6 Modules or TypeScript

// Sweet alert 2
import swal from 'sweetalert2'
window.swal=swal;



const toast = swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});



window.toast = toast;

// supported moment js

const router = new VueRouter({
    // mode: 'history',
    routes, // short for `routes: routes`
    mode: 'hash',
});

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    store,
    components:{adminmain,homemain}
});











