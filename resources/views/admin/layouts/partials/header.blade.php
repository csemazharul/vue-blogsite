<header class="app-header"><a class="app-header__logo" href="index.html">Blog Admin</a>
    <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
    <!-- Navbar Right Menu-->
    <ul class="app-nav">
        <li class="app-search">
            <input class="app-search__input" type="search" placeholder="Search">
            <button class="app-search__button"><i class="fa fa-search"></i></button>
        </li>
        <!--Notification Menu-->

        <!-- User Menu-->
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
            <ul class="dropdown-menu settings-menu dropdown-menu-right">
                <li><a class="dropdown-item" href="{{url('/change-password')}}"><i class="fa fa-cog fa-lock"></i>change password</a></li>
                <li>
                    {{--<a class="dropdown-item" href="page-login.html"><i class="fa fa-sign-out fa-lg"></i> Logout</a>--}}

                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        <i class="fa fa-sign-out fa-lg"></i>{{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>

                </li>
            </ul>
        </li>
    </ul>
</header>
<!-- Sidebar menu-->
<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
    <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" src="" alt="User Image">
        <div>
            <p class="app-sidebar__user-name"></p>
            <p class="app-sidebar__user-designation">Admin</p>
        </div>
    </div>
    <ul class="app-menu">
        <li>
            <router-link to="/home" class="app-menu__item">
                <i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Dashboard</span>
            </router-link>
        </li>

        <li>
            <router-link to="/category-list" class="app-menu__item">
                <i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Category list</span>
            </router-link>
        </li>




        <li class="treeview">
            <router-link to="/post-list" class="app-menu__item">
            <i class="app-menu__icon fa fa-laptop"></i><span class="app-menu__label">Posts</span><i class="treeview-indicator fa fa-angle-right"></i>
            </router-link>
            {{--<ul class="treeview-menu">--}}
                {{--<li><a class="treeview-item" href="{{url('/post/create')}}"><i class="icon fa fa-circle-o"></i>Create</a></li>--}}
                {{--<li><a class="treeview-item" href="{{url('/post')}}"  rel="noopener"><i class="icon fa fa-circle-o"></i>Events</a></li>--}}

            {{--</ul>--}}
        </li>

    </ul>
</aside>