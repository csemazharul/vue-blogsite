<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\Post;

use Intervention\Image\Facades\Image;


class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();
        return response()->json(['posts'=>$posts],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        $this->validate($request,[
//            'title'=>'required|min:5,max:40',
//            'description'=>'required|min:10,max:400',
//            'cat_id'=>'required|',
//            'photo'=>'required|'
//
//        ]);\

        $data=$request->all();

        $strpostion=strpos($request->photo,';');
        $substr=substr($request->photo,0,$strpostion);
        $ex=explode('/',$substr)[1];
        $name=time().'.'.$ex;
        $image= Image::make($request->photo)->resize(800,350);
        $image->save(public_path('/uploadimage/'.$name));
        $data['photo']=$name;
        $data['user_id']=Auth::user()->id;
        Post::create($data);

        return response()->json(['message'=>"succes data store"],200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post=Post::findOrfail($id);
        $imgLocation=public_path('/uploadimage/'.$post->photo);


        if(file_exists($imgLocation))
        {

            unlink($imgLocation);
        }
       $post->delete();
       return response()->json(['message'=>'Post Delete Successfully'],200);
    }
}
