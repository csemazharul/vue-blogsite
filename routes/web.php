<?php


Route::get('/', function () {
    return view('public/index');
});

Route::resource('/post','PostController');
Route::resource('/category','CategoryController');
Route::get('/selected/category/delete/{id}','CategoryController@selectedCategory');
//Route::get('/category-list','CategoryController@categoryList');
Route::get('/data','CategoryController@data');
Route::get('/blogpost','BlogController@getBlogPosts');
Route::get('/latestcategory','BlogController@latestCategories');
Route::get('/singlepost/{id}','BlogController@singlePost');
Route::get('/categories/post/{id}','BlogController@categoryPosts');
Route::get('/search/','BlogController@searchPost');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//Route::get('/{anypath}','HomeController@index')->where('path','.*');