import axios from 'axios'
export default {
    state:{
        category:[],
        posts:[],
        blogposts:[],
        latestCategory:[],
        singlepost:[],

    },
    getters:{
        getCategory(state){
            return state.category;
        },
        getPosts(state)
        {
            return state.posts;
        },
        getBlogPost(state)
        {
            return state.blogposts;
        },
        getLatestCategories(state)
        {
            return state.latestCategory;
        },
        singlePost(state)
        {
            return state.singlepost;
        },

        },
    actions:{
        allCategory(context)
        {
            axios.get('/category')
                .then((response)=>{

                context.commit('categories',response.data.categories)

                })


        },
        allPost(context)
        {
            axios.get('/post')
                .then((response)=>{
                    context.commit('posts',response.data.posts)

                })
        },
        allBlogPost(context)
        {
            axios.get('/blogpost')
                .then((response)=>{

                    context.commit('blogposts',response.data.blogposts)
                })
        },
        latestCategories(context)
        {
            axios.get('/latestcategory')
                .then((response)=>{
                   context.commit('latestCategory',response.data.latestCategory)
                })
        },
        singlepost(context,id)
        {
            axios.get('/singlepost/'+id)
                .then((response)=>{

                    context.commit('singlePost',response.data)

                });
        },

        categoryPosts(context,id)
        {
            axios.get('/categories/post/'+id)
                .then((response)=>{
                context.commit('categoryPost',response.data);
                });
        },
        SearchPost(context,searchValue)
        {
            axios.get('/search/?search='+searchValue)
                .then((response)=>{

                    context.commit('searchPost',response.data);
                });
        }


    },
    mutations:{
        categories(state,data)
        {
             return state.category=data;


        },
        posts(state,collectPost)
        {
            return state.posts=collectPost;
        },
        blogposts(state,collectBlog)
        {
            return state.blogposts=collectBlog;
        },
        latestCategory(state,categoryData)
        {

            return state.latestCategory=categoryData;
        },
        singlePost(state,singleData)
        {
            return state.singlepost=singleData;
        },
        categoryPost(state,categoryPosts)
        {
            return state.blogposts=categoryPosts;
        },
        searchPost(state,searchPosts)
        {
            return state.blogposts=searchPosts;
        },



    }
}